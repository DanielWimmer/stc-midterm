// Daniel Wimmer
// SDS394 Scientific and Technical Computing
// Fall 2016: Midterm Part1

#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "mkl.h"

int main()
{
   // Part1: Multiply Matrices A and B using level 3 Blas routine dgemm

   // Create Matrices A,B,C. Fill A and B with 1's and leave C uninitialized

   int array_size = 2048;
   int i,j,x;
   char normal = 'N';
   double *A,*B,*C;

   A = (double*)mkl_malloc(array_size*array_size*sizeof(double),64);
   B = (double*)mkl_malloc(array_size*array_size*sizeof(double),64);
   C = (double*)mkl_malloc(array_size*array_size*sizeof(double),64);

   for(i=0;i<array_size*array_size;i++)
   {     
      A[i] = 1.0;
      B[i] = 1.0;
   }

   cblas_dgemm(CblasRowMajor,CblasNoTrans,CblasNoTrans,array_size,array_size,1.0,1.0,A,array_size,B,array_size,0.0,C,array_size);

   mkl_free(A);
   mkl_free(B);
   mkl_free(C);

   return 0;
}
