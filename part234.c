// Daniel Wimmer
// SDS394 Scientific and Technical Computing
// Fall 2016: Midterm Part2

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include "mkl.h"
#define A(i,j) A[(i) + array_size* (j)]		// A(i,j) is column major
#define a(i,j) A[array_size*(i) + (j)]		// a(i,j) is row major
#define A_clone(i,j) A[(i) + array_size*(j)]

void factorization(int array_size,double *A,int *ipiv,int info);
void solution(char *trans,int array_size,int one, double *A,int *ipiv,double *b,int info);
void jacobi(int array_size,double *A,double *b,double *x,double *x2);
void gauss(int array_size,double *A,double *b,double *x,double *x3);

int main()
{
   // Build coefficient matrix A that is diagonally dominant
   int array_size = 2048;
   int i,j,info;
   int one = 1;
   double sum;
   char trans[1] = {'N'};

   double *A = mkl_malloc(array_size*array_size*sizeof(double),64);
   double *x = mkl_malloc(array_size*sizeof(double),64);   
   double *b = mkl_malloc(array_size*sizeof(double),64);
   double *x2 = mkl_malloc(array_size*sizeof(double),64);
   double *x3 = mkl_malloc(array_size*sizeof(double),64);
   int *ipiv = mkl_malloc(array_size*sizeof(int),64);

   srand(time(NULL));

   for(i=0;i<array_size*array_size;i++)
      A[i] = (double)rand();

   for(i=0;i<array_size;i++)
   {
      sum = 0.0;
      for(j=0;j<array_size;j++)
         sum = sum + abs(A(i,j));

      A(i,i) = sum;
   }

   // Create random solution vector and multiply by A to create RHS vector b using blas2 vector multiply routine

   for(i=0;i<array_size;i++)
      x[i] = (double)rand();

   cblas_dgemv(CblasColMajor,CblasNoTrans,array_size,array_size,1.0,A,array_size,x,1,0.0,b,1);

   // Use jacobi iteration method

   jacobi(array_size,A,b,x,x2);

   // Use Gauss Siedel iteration method

   gauss(array_size,A,b,x,x3);

   // User LAPACK routine dgetrf to factor A into L and U

   factorization(array_size,A,ipiv,info);
   solution(trans,array_size,one,A,ipiv,b,info);   

   mkl_free(A);
   mkl_free(x);
   mkl_free(b);
   mkl_free(x2);
   mkl_free(ipiv);

   return 0;
}

void factorization(int array_size,double *A, int *ipiv, int info)	//Runs 100 times in order to get enough time to be able to measure
{
   double *A_clone = mkl_malloc(array_size*array_size*sizeof(double),64);
   int iterations,i,j;

   for(i=0;i<array_size;i++)
   {
      for(j=0;j<array_size;j++)
         A_clone(i,j) = A(i,j);
   }
   for(iterations=0;iterations<100;iterations++)
   {
      for(i=0;i<array_size;i++)
      {
         for(j=0;j<array_size;j++)
            A(i,j) = A_clone(i,j);
      }
      dgetrf(&array_size,&array_size,A,&array_size,ipiv,&info);
   }

}

void solution(char *trans,int array_size,int one,double *A,int *ipiv,double *b, int info) 	//Runs 10000 times in order to get enough time to be able to measure
{
   double *b_clone = mkl_malloc(array_size*sizeof(double),64);
   int iterations,i;

   for(i=0;i<array_size;i++)
      b_clone[i] = b[i];

   for(iterations=0;iterations<10000;iterations++)
   {
      for(i=0;i<array_size;i++)
         b[i] = b_clone[i];
   
      dgetrs(trans,&array_size,&one,A,&array_size,ipiv,b,&array_size,&info);
   }
}

void jacobi(int array_size,double *A,double *b,double *x,double *x2)
{
   int i,j,k;
   double error = 1.0;
   double sigma,top,bottom;
   double *x1 = mkl_malloc(array_size*sizeof(double),64);
   double *x_e = mkl_malloc(array_size*sizeof(double),64);

   for(i=0;i<array_size;i++)
   {
      x1[i] = 1.0;
   }

   k = 0;
   
   while(error > 0.00001)
   {
      k++;
      for(i=0;i<array_size;i++)
      {
         sigma = 0;
         for(j=0;j<array_size;j++)
         {
            if(j != i)
               sigma = sigma + A(i,j)*x1[j];
         }
         
         x2[i] = (b[i] - sigma)/A(i,i);
      }
      
      for(i=0;i<array_size;i++)
         x1[i] = x2[i];      

      for(i=0;i<array_size;i++)
         x_e[i] = x[i] - x1[i];
      top = cblas_ddot(array_size,x_e,1,x_e,1);
      bottom = cblas_ddot(array_size,x,1,x,1);
      error = sqrt(top/bottom);
   }

   printf("\n\nNumber of iterations until Jacobi convergence: %d\n", k);
   
   mkl_free(x1);
   mkl_free(x_e);
}

void gauss(int array_size,double *A,double *b,double *x,double *x3)
{
   int i,j,k;
   double error = 1.0;
   double sigma,top,bottom,s;
   double *x1 = mkl_malloc(array_size*sizeof(double),64);
   double *y = mkl_malloc(array_size*sizeof(double),64);
   double *x_e = mkl_malloc(array_size*sizeof(double),64);

   for(i=0;i<array_size;i++)
   {
      x1[i] = 1.0;
      y[i] = 1.0;
   }

   k = 0;

   while(error > 0.00001)
   {
      k++;
      for(i=0;i<array_size;i++)
      {
         sigma = 0.0;
	 s = 0.0;
         for(j=0;j<array_size;j++)
         {
            if(j != i)
            {
               sigma = sigma + A(i,j)*x1[j];
            }
	 }

         x3[i] = (b[i] - sigma)/A(i,i);
         sigma = 0;
         for(j=0;j<array_size;j++)
         {
            if(j < i)
               sigma = sigma + A(i,j)*x3[j];
            if(j > i)
	       sigma = sigma + A(i,j)*x1[j];
         }
         x3[i] = (b[i] - sigma)/A(i,i);
      }

      for(i=0;i<array_size;i++)
         x1[i] = x3[i];

      for(i=0;i<array_size;i++)
         x_e[i] = x[i] - x1[i];
      top = cblas_ddot(array_size,x_e,1,y,1);
      bottom = cblas_ddot(array_size,x,1,y,1);
      error = sqrt((top/bottom) * (top/bottom));
   }

   printf("\n\nNumber of iterations until Gauss Seidel convergence: %d\n\n", k);

   mkl_free(x1);
   mkl_free(y);
   mkl_free(x_e);
}
